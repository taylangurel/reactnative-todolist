import React, {useState} from 'react';
import {Text, View, StyleSheet, FlatList, Alert, TouchableWithoutFeedback, Keyboard, Button, Share } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ListItem from './components/listItem';
import AddItem from './components/addItem';

// npm install [if an error encountered run again :) ]
// npx react-native start // TO START
// npx react-native run-android // TO PROJECT INTO Android EMULATOR [write in a separate terminal while the react-native is working]

//For iOS, it may be needed to run 'npx pod-install' on a MAC machine [for android this is not required] (for AsyncStorage)

export default function TODOList() {

  const [todos, setTodos] = useState([
    {todo: 'Add a todo', key: '1'},
    ]);
  const [needData, setNeedData] = useState(true);

  const saveData = async () => {
    try {
      const stringifiedTodos = JSON.stringify(todos); // To store as string in the AsyncStorage
      await AsyncStorage.setItem('todos', stringifiedTodos);
      console.log('saveData called')
  }
    catch (error) {
      console.log(error)
    }
  }

  getData = async () => {
    try {
      const stringifiedTodos = await AsyncStorage.getItem('todos');
      const todos = JSON.parse(stringifiedTodos);

      if (todos !== null) {
        setTodos(todos);
        console.log('getData called');
        setNeedData(false);
      }
    } catch (error) {
      console.log(error)
    }
  };

  clearData = async () => {
    await AsyncStorage.clear();
    console.log('Cleared AsyncStorage')
  }

  //To get data
  if(needData){
    getData();
  }

  const onShare = async () => {
    const justTodos = todos.map(item => item.todo);
    try {
      const result = await Share.share({
        message:
          JSON.stringify(justTodos)
      });
    } catch (error) {
      alert(error.message);
    }
  };
 
  //This handler DELETES the pressed list item from the list
  const pressHandler = (key) => {
    const newtodos = todos.filter(todo => todo.key != key);
    setTodos(newtodos);
  }

  //ADDS a new todo with the given text and a randomly generated key to the old todos list
  const inputSubmitHandler = (text) => {
    
      if(text.length > 0){
        const newtodos = [{todo: text, key: Math.random().toString()}, ...todos ];
        setTodos(newtodos);
      }else{
        Alert.alert('ERROR!', 'Text cannot be empty', [{text:'OK'}])
      }
  }

  // MARKS the individual tasks as done or not
  const longPressHandler = (key) => {
    const updatedTodos = [...todos];
    const item = updatedTodos.find((x) => x.key == key);
    item.marked = !item.marked;
    setTodos(updatedTodos);
  }

  
  return (
    <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss();}}>
      <View style={styles.mainPage}>
        <View style = {styles.header}>
            <Text style={styles.title}>TODO List</Text>
        </View>
        <View style={styles.content}>
          <AddItem inputSubmitHandler={inputSubmitHandler} />
          <View style={styles.list}>
            <FlatList
              data={todos}
              renderItem={( {item} ) => (
                <ListItem item={item} marked = {item.marked} longPressHandler = {longPressHandler} pressHandler = {pressHandler}/>
              )}
            />
          </View>
          <View style = {styles.buttons}>
            <Button color= 'orange' title={'Save Data'} onPress= {saveData}/>
            <Button color= 'orange' title={'Share'} onPress={onShare}/>
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.infoTexts}>Long press to mark as done</Text>
            <Text style={styles.infoTexts}>Tap to delete</Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

//The margins, paddings, etc. are given as pixel values, wont work same in other devices.
const styles = StyleSheet.create({
  mainPage: {
    flex: 1, // takes the whole background
    backgroundColor: 'white',
  },
  content: {
    flex: 1,
  },
  list:{
    margin: 10,
    flex:1,
  },
  header:{
    height: 50,
    paddingTop: 8,
    backgroundColor: 'orange'
},
  title:{
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
},
  textContainer:{
    justifyContent:"space-between",
    alignContent:"center",
    backgroundColor:'yellow'
},
  infoTexts: {
    textAlign:"center",
    fontSize: 18,
    fontWeight: 'bold'  
},
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: 30,
  }
});

