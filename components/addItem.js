import React, {useState} from 'react';
import {Text, View, StyleSheet, TextInput, Button} from 'react-native';

export default function AddItem ({inputSubmitHandler}) {
    
    const [text, setText] = useState('');

    const inputChangeHandler = (val) => {
        setText(val);
    }

    return(
        <View style={styles.container}>
            <TextInput 
                style={styles.input}
                placeholder='Add new item'
                onChangeText={inputChangeHandler}
            />
            <Button onPress={() => inputSubmitHandler(text)} title='ADD'color='orange'/>
        </View>
    )
}

const styles=StyleSheet.create({
    container:{
        margin: 10,
    },
    input:{
        marginBottom:10,
        borderBottomWidth: 2,
        borderBottomColor: 'orange'
    }
})