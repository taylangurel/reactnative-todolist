import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

export default function ListItem(props) {

    return (
        <TouchableOpacity onLongPress={() => props.longPressHandler(props.item.key)} onPress = {() => props.pressHandler(props.item.key)}>
              <Text style={[styles.listItem, {backgroundColor: props.marked ? 'green' : 'white'},]}> {props.item.todo}</Text> 
        </TouchableOpacity>
    
    )
}

const styles = StyleSheet.create({
    listItem:{
        padding: 8,
        margin:4,
        fontSize: 18,
        textAlignVertical:'center',
        borderColor:'gray',
        borderWidth: 3,
        borderStyle: 'solid',
        borderRadius:10,
      }
})





